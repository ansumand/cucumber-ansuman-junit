package base;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.ExcelReader;
import utilities.ExtentManager;

import java.util.Properties;
import java.util.logging.Logger;

public class Page {
    /*
     * WebDriver
     *
     * ExcelReader
     * Logs
     * WebDriverWait
     * ExtentReports
     *
     *
     *
     */
    public static WebDriver driver;
    public static Properties config = new Properties();
    public static Logger log = Logger.getLogger("devpinoyLogger");
    public static ExcelReader excel = new ExcelReader(
            System.getProperty("user.dir") + "\\src\\test\\resources\\excel\\testdata.xlsx");
    public static WebDriverWait wait;
    //public ExtentReports rep = ExtentManager.getInstance();
    public static ExtentTest test;
    public static String browser;

    public static  void initConfiguration(){
        if(Constants.browser.equals("Firefox")){
            driver = new FirefoxDriver();

         } else if(Constants.browser.equals("Chrome")){
            System.setProperty("webdriver.chrome.driver","src/test/resources/executables/chromedriver.exe");
            driver = new ChromeDriver();
        } else if (Constants.browser.equals("Edge")){
            driver = new EdgeDriver();
        } else {
            driver = new HtmlUnitDriver();
        }


    }
    public static void quitBrowser(){


    }


}
